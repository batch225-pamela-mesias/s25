//JSON OBJECTS

/*
	- JSON stands for Javascript Object Notation
	- JSON is also used in other programming languages hence the name javascript object notation.
	SYNTAX:
	{
	
		"PropertyA": "valueA",
		"PropertyB": "valueB", 
	}

*/

// JSON Objects

/*{
	"city": "quezon city",
	"province": "Metro Manila",
	"country": "Philippines"
}*/

// JSON METHODS
/*
	- The JSON object contains methods for parsing and converting data into stringfied JSON
*/

// CONVERTING DATA INTO STRINGFIED JSON
/*
	- Stringfied JSON is a Javascript object converted into string to be used in other functions of a javascript application.
	- They are commonly is required in HTTP request where information in a stringfied JSON format
	- Request are an important part of programming where application communicates with a backend application to perform different task such as getting/creating data in database.
*/

let batchsArr = [{ batchName: "Batch X" }, { batchName: "Batch Y" }];

// The "stringify" method ised to convert Javascript Object into a string.
console.log("Result from stringify method:");
console.log(JSON.stringify(batchsArr));

//THROUGH OBJECTS STRINGIFY

let data = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city: "Manila",
		country: "Philippines",
	},
});

console.log(data);

// USING STRINGIFY Method with variables
/*
	SYNTAX
	JSON.stringify({
		propertyA: variableA,
		propertyB: variableB
	})
*/

//User details

/*let firstName = prompt("What is your first Name");
let lastName = prompt("What is your last name?");
let age = prompt("what is your age?");
let address = {
	city: prompt("Which city do you live in?"),
	country: prompt("Which country does your city address belong to?")
};


let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})

console.log(otherData);*/

// [Section] Converting stringified JSON into JavaScript objects
/*
	- Objects are common data types used in applications because of the complex data structures that can be created out of them
	- Information is commonly sent to applications in stringified JSON and then converted back into objects
	- This happens both for sending information to a backend application and sending information back to a frontend application
	-Parsing means analyzing and converting a program into an internal format that a runtime environment can actually run
*/

let batchesJSON = `[{ "batchName": "Batch X" }, { "batchName": "Batch Y" }]`;

console.log("Result from parse method:");
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{ "name": "John", "age": "31", "address": { "city": "Manila", "country": "Philippines" } }`;

console.log(JSON.parse(stringifiedObject));
